/* 
 * File:   main.cpp
 * Author: stsdmchv
 */

#include <cstdlib>
#include <iostream>
#include <fstream>

using namespace std;
bool par(int *x, int *y, int count);
bool leks(int *x, int *y, int count, int *mass);
bool equals(int *x, int *y, int count);

int main() {
	bool prov=true, type;
        ifstream in("var7.txt");

	int count=0;
	int temp;

	while (!in.eof())
	{
		in >> temp;
		count++;
	}
        in.clear();
	in.seekg(0, ios::beg);

	bool endline=false;
	int column=0;
	char symbol;
	while (!in.eof()) {
		in.get(symbol);
		if ( (symbol == ' ' || symbol == '\n') && endline) {
			column++;
			endline=false;
			if (symbol == '\n') break;
		}
		if (symbol != ' ') {
			endline=true;
		}
	}
	in.clear();
	in.seekg(0, ios::beg);

	int n=count/column;

	int **x;
	x = new int*[n];
	for (int i=0;i<n;++i) x[i] = new int[column];
	for (int i=0;i<n;++i)
		for (int j=0;j<column;++j)
			in >> x[i][j];

	for (int i=0;i<n;++i) {
		for (int j=0;j<column;++j)
			cout << x[i][j] << "\t";
		cout << "\n";
	}
	in.close();

	bool *s = new bool[n];
	for (int i=0;i<n;++i) s[i] = true;
	
	int *mass= new int[column];
	mass[0] = 1;
	mass[1] = 0;
	mass[2] = 2;
	cout << "0 - метод Паретто\n1 - метод лексикографии\n\tВыберите метод: ";
	cin >> type;
	switch(type){
            case 0: {
		for (int i=0;i<n-1;++i) {
			if (s[i]) {
				for (int j=i+1;j<n;++j) {
					if (par(x[i], x[j], column) && s[j]) s[j] = false;
					else
						if ((par(x[j], x[i], column) || equals(x[j], x[i], column)) && s[i] && s[j]) {
							s[i] = false;
							break;
						}
				}
			}
		}
            break;
            }
            
            case 1:{
                for (int i=0;i<n-1;++i)	{
                        if (s[i]) {
                                for (int j=i+1;j<n;++j) {
                                        if (leks(x[i], x[j], column, mass) && s[j]) {
                                                s[j] = false;
                                        }
                                        else
                                                if ((leks(x[j], x[i], column, mass) || equals(x[j], x[i], column)) && s[i] && s[j]) {
                                                        s[i] = false;
                                                        break;
                                                }
                                }
                        }
                }
            break;
            }
            
        }
	for (int i=0;i<n;++i){
		if (s[i]) {
			cout << "N=" << i+1 << " { ";
			for (int j=0;j<column;++j) {
				cout << x[i][j] << " ";
			}
			cout << "}" << endl;
		}
	}
	return 0;
}

bool par(int *x, int *y, int count) {
	bool k, l;
	k=true; l=false;
	for (int i=0;i<count;++i) {
		if (x[i] <= y[i]) {
			if (x[i]<y[i]) {
				l=true;
			}
		}
		else k=false;
	}
	if (l == true && k == true) return true;
	return false;
}


bool leks(int *x, int *y, int count, int *mass) {
	bool k=true, l=false;
	for (int i=0;i<count;++i) {
		if (x[mass[i]] <= y[mass[i]]) {
			if (x[mass[i]] < y[mass[i]]) {
				l=true;
				break;
			}
		}
		else k=false;
	}
	if (l == true && k == true) return true;
	return false;
}

bool equals(int *x, int *y, int count) {
	for (int i=0;i<count;++i)
		if (x[i] != y[i]) return false;
	return true;
}
